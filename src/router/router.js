import Vue from 'vue';
import VueRouter from 'vue-router';

import Login from '@/components/Login';
import Employers from '@/components/Employers';
import CreateEmployer from '@/components/CreateEmployer';
import SearchEmployers from '@/components/SearchEmployers';
import UpdateEmployer from '@/components/UpdateEmployer';

Vue.use(VueRouter);

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Login,
            name: 'login',
        },
        {
            path: '/employers',
            component: Employers,
            name: 'Employers',
        },
        {
            path: '/employers/create',
            component: CreateEmployer,
            name: 'CreateEmployer',
        },
        {
            path: '/employers/:id',
            component: UpdateEmployer,
            name: 'UpdateEmployer',
        },
        {
            path: '/employers/search',
            component: SearchEmployers,
            name: 'SearchEmployers',
        }
    ]
});