import axios from 'axios';

const BASE_URL = process.env.VUE_APP_BASE_URL;

export default {
    async index(token, page) {
        return await axios.get(`${BASE_URL}/employers?page=${page}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            },
        });
    },
    save(token, employer) {
        return axios.post(`${BASE_URL}/employers`, JSON.stringify(employer), {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            },
        });
    },
    async search(token, search) {
        return await axios.post(`${BASE_URL}/employers/search`, {
            search: search,
        }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            },
        });
    },
    getEmployer(token, id) {
      return axios.get(`${BASE_URL}/employers/${id}`, {
          headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`,
          },
      })
    },
    updateEmployer(token, id, payload) {
      return axios.put(`${BASE_URL}/employers/${id}`, JSON.stringify(payload), {
          headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`,
          },
      })
    },
    deleteEmployer(token, id) {
      return axios.delete(`${BASE_URL}/employers/${id}`,{
          headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`,
          },
      })
    },
};