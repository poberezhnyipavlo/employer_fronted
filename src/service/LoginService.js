import axios from 'axios';

const BASE_URL = process.env.VUE_APP_BASE_URL;

export default {
    async login(form) {
        return await axios.post(`${BASE_URL}/login/`, JSON.stringify(form), {
            headers: {
                'Content-Type': 'application/json'
            },
        });
    },
}