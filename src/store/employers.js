import EmployerService from '@/service/EmployerService';

const SET_EMPLOYERS = 'SET_EMPLOYERS';
const SET_SEARCH_EMPLOYERS = 'SET_SEARCH_EMPLOYERS';
const SET_EMPLOYER = 'SET_EMPLOYER';

export default ({
    namespaced: true,
    state: {
        employers: [],
        paginate: {
            total: 10,
            count: 10,
            perPage: 10,
            currentPage: 1,
            totalPages: 1,
        },
        searchEmployers: [],
        employer: {
            fname: '',
            sname: '',
            pname: '',
        }
    },
    actions: {
        setEmployers({commit}, payload) {
            EmployerService.index(payload.token, payload.id)
                .then((response) => {
                    commit(SET_EMPLOYERS, response.data);
                })
        },
        setSearchEmployers({commit}, payload) {
            EmployerService.search(payload.token, payload.search)
            .then((response) => {
                commit(SET_SEARCH_EMPLOYERS, response.data);
            })
        },
        setEmployer({commit}, payload) {
            EmployerService.getEmployer(payload.token, payload.id)
            .then((response) => {
                commit(SET_EMPLOYER, response.data);
            })
        }
    },
    mutations: {
        [SET_EMPLOYERS](state, payload) {
            state.employers = payload.data;
            state.paginate.count = payload.paginate.count;
            state.paginate.total = payload.paginate.total;
            state.paginate.perPage = payload.paginate.per_page;
            state.paginate.currentPage = payload.paginate.current_page;
            state.paginate.totalPages = payload.paginate.total_pages;
        },
        [SET_SEARCH_EMPLOYERS](state, payload) {
            state.searchEmployers = payload;
        },
        [SET_EMPLOYER](state, payload) {
            state.employer = payload
        }
    },
});
