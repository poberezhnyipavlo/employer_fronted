import Vue from 'vue'
import Vuex from 'vuex'

import user from '@/store/user';
import employers from '@/store/employers';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        user,
        employers,
    }
});