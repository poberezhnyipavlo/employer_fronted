const SET_TOKEN = 'SET_TOKEN';

export default ({
    namespaced: true,
    state: {
        token: localStorage.getItem('token') || '',
    },
    getters: {
        isAuthenticated(state) {
            return !!state.token;
        }
    },
    actions: {
        setToken({commit}, token) {
            localStorage.setItem('token', token)
            commit(SET_TOKEN, token)
        },
    },
    mutations: {
        [SET_TOKEN](state, token) {
            state.token = token;
        },
    },
});
